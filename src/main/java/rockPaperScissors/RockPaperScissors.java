package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

        while(true) {
            System.out.println("Let's play round " + roundCounter);

            String yourChoice = readInput("Your choice (Rock/Paper/Scissors)? "); // Ask for input from user.
            yourChoice = yourChoice.toLowerCase();
            
            Random r = new Random();
            int randomitem = r.nextInt(rpsChoices.size());
            String computerChoice = rpsChoices.get(randomitem);
            computerChoice = computerChoice.toLowerCase();

            if (yourChoice.equals(computerChoice)) {
                System.out.println("Human chose " + yourChoice + ", computer chose " + computerChoice + ". It's a tie!");
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }
            else if(yourChoice.equals("rock")) {
                if (computerChoice.equals("scissors")) {
                    System.out.println("Human chose " + yourChoice + ", computer chose " + computerChoice + ". Human wins!");
                    humanScore ++;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
                else if (computerChoice.equals("paper")) {
                    System.out.println("Human chose " + yourChoice + ", computer chose " + computerChoice + ". Computer wins!");
                    computerScore ++;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
            }
            else if (yourChoice.equals("paper")) {
                if (computerChoice.equals("scissors")) {
                    System.out.println("Human chose " + yourChoice + ", computer chose " + computerChoice + ". Computer wins!");
                    computerScore ++;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
                else if (computerChoice.equals("rock")) {
                    System.out.println("Human chose " + yourChoice + ", computer chose " + computerChoice + ". Human wins!");
                    humanScore ++;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
            }
            else if (yourChoice.equals("scissors")) {
                if (computerChoice.equals("paper")) {
                    System.out.println("Human chose " + yourChoice + ", computer chose " + computerChoice + ". Human wins!");
                    humanScore ++;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
                else if (computerChoice.equals("rock")) {
                    System.out.println("Human chose " + yourChoice + ", computer chose " + computerChoice + ". Computer wins!");
                    computerScore ++;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
            }
            else {
                System.out.println("I do not understand "+ yourChoice + " Could you try again?");
            }

            String stop = readInput("Do you wish to continue playing? (y/n)? ");
            stop = stop.toLowerCase();

            if(stop.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
            roundCounter ++;
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
